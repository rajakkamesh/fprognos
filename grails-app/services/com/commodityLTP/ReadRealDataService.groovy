package com.commodityLTP

import com.commodityOHLC.WriteOhlcService
import grails.transaction.Transactional
import org.apache.poi.hssf.usermodel.HSSFCell
import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook


import java.text.SimpleDateFormat

//import for xls file reading
@Transactional
class ReadRealDataService {


    def serviceMethod(params) {

        def cruLTP = new CrudeLTP()
        System.out.println(cruLTP.list().dateTime[0])
        def golLTP = new CrudeLTP()
        System.out.println(golLTP.list().dateTime[0])


        try {
            def row = 1;
            FileInputStream fileInputStream = new FileInputStream(params.filepath);
            HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
            HSSFSheet worksheet = workbook.getSheetAt(0);


            for(;;){

                while(row<8){
                    HSSFRow row1 = worksheet.getRow(row);
                    //commodity name
                    HSSFCell CommodityName = row1.getCell(1);
                    //LTP value
                    HSSFCell LTP = row1.getCell(7);
                    def saveLTP = LTP.getNumericCellValue();
                    //date time
                    HSSFCell LTPTime = row1.getCell(16);
                    def saveLTPTIME =  new SimpleDateFormat("hh:mm:ss").format(LTPTime.getDateCellValue());

                    def comname = CommodityName.getStringCellValue().toString().split(" ");
                    System.out.println("COMMODITY : " + comname[0]);
                    System.out.println("LTP : " + saveLTP);

                    System.out.println("LTP TIME : "+new SimpleDateFormat("MM/dd/yyyy").format(new Date())+" "+saveLTPTIME);
                    System.out.println("");
                    System.out.println("");
                    //hh:mm:ss




                    if (comname[0].equals("CRUDEOIL")){
                        CrudeLTP crudeLTP
                        crudeLTP = new CrudeLTP()
                        crudeLTP.lastTradedPrice = saveLTP
                        crudeLTP.dateTime = new SimpleDateFormat("MM/dd/yyyy").format(new Date())+" "+saveLTPTIME
                        crudeLTP.clockStatus= false
                        crudeLTP.save(flush: true,failOnError: true)




                    }
                    if (comname[0].equals("GOLD")){
                        def goldLTP = new GoldLTP()
                        goldLTP.lastTradedPrice = saveLTP
                        goldLTP.dateTime = new SimpleDateFormat("MM/dd/yyyy").format(new Date())+" "+saveLTPTIME
                        goldLTP.clockStatus= false
                        goldLTP.save(flush: true,failOnError: true)

                    }








                    sleep(4000);
                    row++;
                }
                row=1;

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}



