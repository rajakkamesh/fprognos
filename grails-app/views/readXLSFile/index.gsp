<%--
  Created by IntelliJ IDEA.
  User: kamesh
  Date: 23/10/15
  Time: 2:48 PM
--%>

<%@ page import="org.junit.experimental.categories.Categories" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="jarvis"/>
</head>

<body>
<div class="container">

<g:form controller="ReadXLSFile" action="readXLS">

    <table class="table table-responsive">




        <tr>
            <td><label for="filepath">realData File Path :</label></td>
            <td><g:textField name="filepath" maxlength="255" > </g:textField></td>
            </tr>

        <tr>
            <td>
                <label>Select Commodities :</label>
            </td>
            <td>
%{--${commodity[0]}--}%
%{--${commodity[1]}--}%
<g:select name="selectedCommodity" from="${commodity}" optionKey="" optionValue="" multiple="true"  />

            </td>
        </tr>
        <tr>
            <td><label for="candlesize">Candle Size :</label></td>
            <td><g:textField name="candlesize" maxlength="255" > </g:textField></td>
        </tr>
        <tr>
            <td><label for="parameter1">Parameter One :</label></td>
            <td><g:textField name="parameter1" maxlength="255" > </g:textField></td>

        </tr>
        <tr>
        <td><label for="parameter2">Parameter Two :</label></td>
        <td><g:textField name="parameter2" maxlength="255" > </g:textField></td>
        </tr>


        <tr>
            <td><input value="Submit" type="submit" class="btn btn-primary"></td>
            <td></td>
        </tr>



    </table>





</g:form>

</div>

</body>
</html>