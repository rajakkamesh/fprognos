package com.commodityLTP

class GoldLTP {

    String dateTime
    int lastTradedPrice
    Boolean clockStatus




    static constraints = {
        dateTime(nullable: false)
        lastTradedPrice(unique: false)
        clockStatus(nullable: false)
    }
}
