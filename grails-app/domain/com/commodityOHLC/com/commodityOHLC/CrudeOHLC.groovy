package com.commodityOHLC.com.commodityOHLC

class CrudeOHLC {
    String date;
    String time;
    float open;
    float high;
    float low;
    float close;
    float ema1;
    float ema2;
    float crossover;


    static constraints = {


        date(nullable: false)
        time(nullable: false)


    }
}
